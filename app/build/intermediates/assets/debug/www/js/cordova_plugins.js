cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "../plugins/JPushPlugin.js",
        "id": "cn.jpush.phonegap.JPushPlugin.JPushPlugin",
        "clobbers": [
            "window.plugins.jPushPlugin"
        ]
    },
    {
        "file": "../plugins/device.js",
        "id": "org.apache.cordova.device.device",
        "clobbers": [
            "device"
        ]
    }
    ,
    {
        "file": "../plugins/share.js",
        "id": "cn.jpush.phonegap.Share",
        "clobbers": [
            "Share"
        ]
    }
];
module.exports.metadata = 
{
    "cn.jpush.phonegap.JPushPlugin": "2.0.0",
    "org.apache.cordova.device": "0.3.0",
    "cn.jpush.phonegap.Share": "0.3.0"
}
});