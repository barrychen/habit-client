
//页面初始化
$(document).on("pageinit", "#withTeamPage", function(event) {
	if (!habitListPageIsloaded) {
		$("#userName").html(getUserName()+"<span class='caret'></span>");
		habitListPageIsloaded = true;
		var params=getParamFromUrl(event);
		habitListGetAjaxData(habitListPagePageSize,true);
	}
});


//取得用户所有的habits
var habitListPageIsloaded = false;// 防止执行重复
var habitListPagePageSize = 20;
var habitListPagePageNo = 0;
var habitListPageNextpageNo = 0;
var habitListPageTotalPageCount = 0;
var habitListPageTotalCount = 0;
function habitListGetAjaxData(habitListPagePageSize,isempty){
	var userId=getLoginId();
	$.ajax({
			timeout : 2000,// 请求超时时间（毫秒）
			type: "POST", 
			data: {"pageNo":habitListPageNextpageNo,"pageSize":habitListPagePageSize,"userId":getLoginId()}, 
			async : false,// 同步
			dataType : "json",// 返回json格式的数据
			beforeSend : function() {
				showLoader();
			},
			complete : function() {
				hideLoader();
			},
			url : appUrl+"/habit/getHabitVOList.do",
			success : function(json) {
				if(json.result=="success"){
					var data=json.data;			
					var dataList= data.dataList;	
					//var len = dataList.length;
					if(isempty){
						$("#habits-container").empty();
					};
					for ( var key in dataList) {
						var data=dataList[key];
						var friendProgress="";
						if(data.approved==1 && data.friendProgress!="" && data.friendProgress!="undefined" && data.friendId!=data.userId){
							friendProgress="<div class='progress'><div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='"+data.friendProgress+"' aria-valuemin='0' aria-valuemax='21' style='width:"+parseInt(data.friendProgress)/21*100+"%'' id='fprogress-"+data.id+"'></div></div>";
						}
						$("#habits-container").prepend("<a href='#' class='list-group-item' onclick='checkin("+data.id+")'>"+data.content+"<span class='label label-warning pull-right'>"+data.progressStr+"</span>"+
															"<div class='progress'><div class='progress-bar progress-bar-info' role='progressbar' aria-valuenow='"+data.progress+"' aria-valuemin='0' aria-valuemax='21' style='width:"+parseInt(data.progress)/21*100+"%'' id='progress-"+data.id+"'></div></div>"+
															friendProgress+
														"</a>");
					}
				}else{
					alert("设置信息失败！");
				}
				//pactListScrollBind();
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert("异常信息：" + textStatus);
			}
		});
}


