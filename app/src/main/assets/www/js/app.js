$( document ).bind( "mobileinit", function() { 
   $.support.cors = true;    
   $.mobile.allowCrossDomainPages = true; 
}); 

$( document ).on( "tap", "#infoPageBtn", function( e ) {
	$.mobile.changePage( "info.html", { transition: "slideup",reloadPage:true} , true, true);
});

$( document ).on( "tap", "#homePageBtn", function( e ) {
	$.mobile.changePage( "index.html", { transition: "slideup",reloadPage:true} , true, true);
});