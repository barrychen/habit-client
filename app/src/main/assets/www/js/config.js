//定义服务器访问地址
var appUrl="http://10.19.47.254:8080";

//for debug
var loginId=23;
var nickname;


function getParamFromUrl(e) {
	var url = e.target.baseURI;
	// alert(url);
	if (typeof url !== "string") {
		return;
	}
	// var paramUrl = $.mobile.path.parseUrl(e.target.baseURI);
	// var params = getUrlParam(paramUrl.search);
	var params = getUrlParam(url);
	return params;
}


//取得登录用户
function getPlayer(){
	var player=window.localStorage.getItem("player");
	return JSON.parse(player);
}

//清除登录用户
function clearPlayer(){
	window.localStorage.clear();
}

//设置登录用户
function setPlayer(player){
	window.localStorage.setItem("player",JSON.stringify(player));
}

//取得登录用户id
function getLoginId(){
	return getPlayer().id;
}

//取得登录用户昵称
function getUserName(){
	return getPlayer().username;
}

function getUrlParam(string) {
	var obj = {};
	if (string.indexOf("?") != -1) {
		var string = string.substr(string.indexOf("?") + 1);
		var strs = string.split("&");
		for (var i = 0; i < strs.length; i++) {
			var tempArr = strs[i].split("=");
			obj[tempArr[0]] = unescape(tempArr[1]);
		}
	}
	return obj;
}

function updateConfigPlayer(){
	$.ajax({
		timeout : 2000,// 请求超时时间（毫秒）
		async : true,// 异步
		dataType : "json",// 返回json格式的数据
		beforeSend : function() {
			showLoader();
		},
		complete : function() {
			hideLoader();
		},
		url : appUrl + "player/get.do?id=" + loginId,
		success : function(json) {
			if (json.result == "success") {
				//var data = json.data;
				var player=json.data;
				setPlayer(player);
				teamId=player.teamId;
			}

		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("异常信息：" + textStatus);
			// $.mobile.changePage("error.html","slidedown",
			// true,
			// true);
			// this; //调用本次ajax请求时传递的options参数
		}
	});
	
}
