
/**
*   注册事件
**/
$( document ).on( "tap", "#loginBtn", function( e ) {
	var loginName=$("#username").val();
	var password=$("#password").val();
	if(loginName==""){
		swal("Failed!", "input your email!", "error");
		return;
	}
	if(password==""){
		swal("Failed!", "input your password!", "error");
		return;
	}
	login(loginName,password);
});

/**
* 验证输入的账号是否已经被注册
*/
function login(email,password){
	// ///Params.....
	$.ajax({
		timeout : 2000,// 请求超时时间（毫秒）
		type: "POST", 
		data: {"email":email,"password":password}, 
		async : false,// 同步
		dataType : "json",// 返回json格式的数据
		beforeSend : function() {
			showLoader();
		},
		complete : function() {
			hideLoader();
		},
		url : appUrl+"/player/login.do",
		success : function(json) {
			if(json.result=="success"){
				var player=json.data;
				setPlayer(player);
				$.mobile.changePage( "index.html", { transition: "slideup",reloadPage:true} , true, true);
			}else{
				alert(json.messages[0]);
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("异常信息：" + textStatus);
		}
	});
}

/**
*   注册事件
**/
$( document ).on( "tap", "#regBtn", function( e ) {
	var userName=$("#username_reg").val();
	var password=$("#password_reg").val();
	var email=$("#email_reg").val();
	//$.mobile.changePage( "playerInfo/playerInfoEdit.html", { transition: "slideup",reloadPage:true} , true, true);
	//输入验证
	if(checkData(email,password,userName)){
		//判断用户名是否已被注册
		checkExist(email,password,userName);
	}
});

/**
*   校验
**/
function checkData(email,password,userName){
	if(email==""){
		swal("Failed!", "input your email!", "error");
		return false;
	}
	
	if(userName==""){
		swal("Failed!", "input your username!", "error");
		return false;
	}
	
	if(password==""){
		swal("Failed!", "input your password!", "error");
		return false;
	}
	return true;
}

/**
* 验证输入的账号是否已经被注册
*/
function checkExist(email,password,userName){
	// ///Params.....
	$.ajax({
		timeout : 2000,// 请求超时时间（毫秒）
		type: "POST", 
		data: {"email":email}, 
		async : false,// 同步
		dataType : "json",// 返回json格式的数据
		beforeSend : function() {
			showLoader();
		},
		complete : function() {
			hideLoader();
		},
		url : appUrl+"/player/checkExist.do",
		success : function(json) {
			if(json.result=="success"){
				regPlayerAjaxData(email,password,userName);
			}else{
				swal("Failed!", "该手机号已被注册!", "error");
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("异常信息：" + textStatus);
		}
	});
}

/**
*发送注册请求
**/
function regPlayerAjaxData(email,password,userName) {
	$.ajax({
		timeout : 2000,// 请求超时时间（毫秒）
		type: "POST", 
		data: {"email":email,"password":password,"userName":userName}, 
		async : false,// 同步
		dataType : "json",// 返回json格式的数据
		beforeSend : function() {
			showLoader();
		},
		complete : function() {
			hideLoader();
		},
		url : appUrl+"/player/reg.do",
		success : function(json) {
			if(json.result=="success"){
				var playVO=json.data;
				setPlayer(playVO);
				$.mobile.changePage( "index.html", { transition: "slideup",reloadPage:true} , true, true);
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("异常信息：" + textStatus);
		}
	});
}