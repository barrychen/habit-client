function showLoader() {
	// 显示加载器.for jQuery Mobile 1.2.0 以上
	$.mobile.loading('show', {
		text : '数据加载中，请稍候', // 加载器中显示的文字
		textVisible : true, // 是否显示文字
		theme : 'a', // 加载器主题样式a-e
		textonly : false, // 是否只显示文字
		html : "" // 要显示的html内容，如图片等，默认使用Theme里的ajaxLoad图片
	});
}
function hideLoader() {
	// 隐藏加载器
	$.mobile.loading('hide');
}