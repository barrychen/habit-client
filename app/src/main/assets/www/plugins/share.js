var Share = {
	//分享到微信
		shareWeixinImg : function(content, success, fail) {
		return cordova.exec(success, fail, 'Share', 'shareWeixinImg', [ content ]);
	},
	//分享到qq空间
	shareQZone : function(content, success, fail) {
		return cordova.exec(success, fail, 'Share', 'shareQZone', [ content ]);
	},
	shareQQ : function(content, success, fail) {
		return cordova.exec(success, fail, 'Share', 'shareQQ', [ content ]);
	},
	//分享到新浪微博
	shareSina : function(content, success, fail) {
		return cordova.exec(success, fail, 'Share', 'shareSina', [ content ]);
	},
	//分享到腾讯微博
	shareTencent : function(content, success, fail) {
		return cordova
				.exec(success, fail, 'Share', 'shareTencent', [ content ]);
	}
};
