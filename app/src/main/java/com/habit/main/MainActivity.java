package com.habit.main;

import org.apache.cordova.Config;
import org.apache.cordova.DroidGap;

import android.os.Bundle;

import android.view.Menu;


public class MainActivity extends DroidGap {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setIntegerProperty("splashscreen", R.drawable.splash_sn);
		super.setIntegerProperty("loadUrlTimeoutValue", 60000);
		super.loadUrl(Config.getStartUrl(), 4500);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}



}
